@echo off
set KINDLE=%LocalAppData%\Amazon\Kindle
if not exist "%KINDLE%\application\Kindle.exe" goto :nokindle
if exist "%KINDLE%\storage" if not exist "%KINDLE%\storage\" del /Q "%KINDLE%\storage"
if exist "%KINDLE%\updates" rmdir /S /Q "%KINDLE%\updates"
echo This file disables Kindle for PC downloads. > "%KINDLE%\updates"
echo Kindle for PC downloads are now disabled
goto :exit
:nokindle
echo Cannot disable downloads - Kindle for PC is not installed at expected location
:exit
pause